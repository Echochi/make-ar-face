# Draw Make, Root or Unity logos on your face, and wear some shades!

## Content

This is a project file which contains scripts from XZIMG which does facial recognition and AR mapping. It has a sample scene file which fits face masks on up to 3 people and glasses on the first person matched.

![makear.png gives you an idea of how this shoud work.](https://bytebucket.org/Echochi/make-ar-face/raw/8231225204d5cbb89af7d5c6a481e61f0840e28b/makear.png)

> _Note: This repo is slow to dowload due to the material texture files!_

## Setup

**1. Set up Unity**

 In order to run this you need to install Unity using the [setup instructions for Unity](https://github.com/OfferZen-Make/arinsuretech-activity-unity-setup). Vuforia is not used but is still fun to have. It's extremely helptful to clone the [kicker project](https://github.com/OfferZen-Make/arinsuretech-kicker) and follow along with the [tutorial](https://bitbucket.org/waydemlyle/makeday/src/master/) to get a good feel for how Unity works.  
 
**2. Get XZIMG working**
 
 Already included in this project is the [XZIMG Magic Face trial version](https://www.xzimg.com/Downloads). This includes the entire project file.

**3. Aquire sweet sweet swag**

For your cool shades you need models and textures. It's included, but the model is a [free glasses .obj file](https://free3d.com/3d-model/glasses-56919.html), and materials added were [metal textures](https://assetstore.unity.com/packages/2d/textures-materials/real-materials-vol-0-free-115597?aid=1011lGbg&utm_source=aff) and [glass textures](https://assetstore.unity.com/packages/vfx/shaders/glass-and-crystals-shader-109815?aid=1011lGbg&utm_source=aff) imported from the Asset library in Unity. 

<small> The glass texture file `~/Assets/FXVGlassyShader/Scripts/Other/FXVSaveScreenshot.cs` needed an update to work with the newest version of Unity.</small>

## To Play

Open a new Project by navigating to the project file. In the bottom `project` window, navigate to `>Assets/Scene/` and double click on the `sample-magicFace3DFeatures.unity` file. Click Play! Provided your webcam is set up correctly it should just work.

## Modifying Files

**1. Add models**

You can download free models from the [Unity Asset Store](https://assetstore.unity.com/) or use .obj and .fdx files from other free sites such as [Free3D](https://free3d.com/). Drag the unzipped model into yuor project `Assets` folder, or import it from Unity. 

<small> To add it to your scene, drag it into the left-hand panel under the Pivot. You can reposition and resize it using the top left-hand navigation buttons. To figure out where the AR face will appear relative to the new model, it's helpful to use the position of the glasses model, so double click on the glasses or your model in the left menu to find them in the scene. You can add whatever you like to the pivot and deselect models you don't want to see using the tick box on the right hand panel.</small>

**2. Change face masks**

Select the `magicFace3DFeatures` object in the left menu. In the right menu you can modify element 0-2 which correspond to the 3 different faces.

<small> To add different pictures, drag jpeg's/png's from the `Assets/Resources/` folder from the bottom menu into the `Render Texture` form field in the right menu.</small>

**3. And more!**

Read the Magic Face [documentation](https://www.xzimg.com/Docs) and map the vertices onto your image to accurately project a face onto the mesh. 

Update the the shader `~/Assets/Resources/Shaders/Face3DShaderTransparent.shader` to modify opacity by changing the `_Color` variable, or other changes you can think of.

Please Share your changes and improvements! 

Hope you enjoy!